package com.example.maury.hotelcalifornia.fragments;

import android.os.Bundle;
import android.support.v4.content.PermissionChecker;

import com.example.maury.hotelcalifornia.models.Place;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.security.Permission;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class PlacesMapFragment extends SupportMapFragment implements OnMapReadyCallback{
    private GoogleMap map;
    private HashMap<String, Marker> places_marker_map = new HashMap<>();

    public static final LatLng MEJORANDOLA = new LatLng(4.667184, -74.059463);
    private final static String PLACES_FILE_NAME = "hotels.json";

    @Override
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap map) {
        map.setMyLocationEnabled(true);
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(MEJORANDOLA, 16));
        map.getUiSettings().setZoomControlsEnabled(false);
        ArrayList<Place> places = loadPlacesFromAssets();

        // Agregando los markers
        for(Place place: places){
            Marker marker = map.addMarker(new MarkerOptions()
                    .position(place.getLatLng())
                    .title(place.getTitle())
                    .snippet(place.getDescription())
                    .icon(BitmapDescriptorFactory.fromResource(place.getResourceMarker()))
            );

            places_marker_map.put(place.getTitle(), marker);
        }
    }

    private ArrayList<Place> loadPlacesFromAssets(){
        StringBuilder builder = new StringBuilder();
        BufferedReader reader;

        try{
            reader = new BufferedReader(new InputStreamReader(getActivity().getAssets().open(PLACES_FILE_NAME)));
            String line = "";
            while((line = reader.readLine()) != null){
                builder.append(line);
            }
            reader.close();
        } catch(IOException e){
            e.printStackTrace();
        }

        String json = builder.toString();

        Gson gson = new Gson();
        return new ArrayList<>(Arrays.asList(gson.fromJson(json, Place[].class)));
    }
}
