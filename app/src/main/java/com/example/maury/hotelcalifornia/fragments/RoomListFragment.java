package com.example.maury.hotelcalifornia.fragments;

import android.support.v4.app.ListFragment;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.maury.hotelcalifornia.R;
import com.example.maury.hotelcalifornia.RoomDetailActivity;
import com.example.maury.hotelcalifornia.data.CustomAdapter;
import com.example.maury.hotelcalifornia.models.Room;

import java.util.ArrayList;

public class RoomListFragment extends ListFragment {
    private SwipeRefreshLayout swipeContainer;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_room_list, container, false);

        // Agregando el pull to refresh
        swipeContainer = (SwipeRefreshLayout) view.findViewById(R.id.swipeContainer);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Setup refresh listener which triggers new data loading
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Your code to refresh the list here.
                // Make sure you call swipeContainer.setRefreshing(false)
                // once the network request has completed successfully.
                fetchTimelineAsync(0);
            }
        });
        // Configure the refreshing colors
        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        // Como el activity hereda de ListActivity, con este metodo ya tengo la lista
        // porque estoy usando el ID generico en el layout
        ListView list = getListView();

        ArrayList<Room> rooms = new ArrayList<>();

        // Recorro los ARRAY que tienen los cuartos y a partir de ellos lleno mi arraylist
        for(String room: getResources().getStringArray(R.array.array_rooms_standard)){
            Room one_room = new Room(room, Room.STANDARD_ROMM);
            rooms.add(one_room);
        }

        for(String room: getResources().getStringArray(R.array.array_rooms_luxury)){
            Room one_room = new Room(room, Room.LUXURY_ROMM);
            rooms.add(one_room);
        }

        // Creando el adapter con los datos y asignandolo a mi lista
        CustomAdapter adapter = new CustomAdapter(getActivity(), rooms, true);
        setListAdapter(adapter);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        Room clicked_room = (Room) l.getItemAtPosition(position);
        Intent intent = new Intent(getActivity(), RoomDetailActivity.class);
        intent.putExtra(RoomDetailActivity.ROOM_TYPE, clicked_room.getRoomType());
        intent.putExtra(RoomDetailActivity.ROOM_NUMBER, clicked_room.getRoomNumber());
        startActivity(intent);
    }

    public void fetchTimelineAsync(int page) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                try{
                    Thread.sleep(3000);
                }catch(InterruptedException e){
                    //
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);

                swipeContainer.setRefreshing(false);
            }
        }.execute();
    }

}
