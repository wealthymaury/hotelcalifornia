package com.example.maury.hotelcalifornia.fragments;

import android.app.ActionBar;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.maury.hotelcalifornia.R;
import com.example.maury.hotelcalifornia.data.CustomPagerAdapter;

public class MainFragment extends Fragment implements ViewPager.OnPageChangeListener, ActionBar.TabListener{
    private ViewPager view_pager;
    private CustomPagerAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        view_pager = (ViewPager) view.findViewById(R.id.pager);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        adapter = new CustomPagerAdapter(getActivity().getSupportFragmentManager());
        view_pager.setAdapter(adapter);

        // Haciendo que la clase que responde a los eventos de swipe sea esta misma, por el
        // implements del ViewPager.OnPageChangeListener
        view_pager.setOnPageChangeListener(this);

        ActionBar bar = getActivity().getActionBar();
        bar.removeAllTabs();
        bar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        // al crear el tab, se busca que esta misma clase responda ante los eventos de tab
        // Por el implements de ActionBar.TabListener
        bar.addTab(bar.newTab().setText("Lista").setTabListener(this));
        bar.addTab(bar.newTab().setText("Grid").setTabListener(this));
        bar.addTab(bar.newTab().setText("Mapa").setTabListener(this));
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onPageSelected(int position) {
        getActivity().getActionBar().setSelectedNavigationItem(position);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {

    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
        view_pager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {

    }
}
