package com.example.maury.hotelcalifornia.data;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import com.example.maury.hotelcalifornia.R;
import com.example.maury.hotelcalifornia.models.Room;

public class CustomAdapter extends ArrayAdapter<Room> {
    ArrayList<Room> data;
    LayoutInflater inflater;
    boolean is_list;

    public CustomAdapter(Context context, ArrayList<Room> objects, boolean is_list){
        super(context, -1, objects);
        this.data = objects;
        this.inflater = LayoutInflater.from(context);
        this.is_list = is_list;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        Room current_room = data.get(position);
        String room_type = current_room.getRoomType();

        int image_resource;
        if(room_type.equals(Room.STANDARD_ROMM)){
            image_resource = R.drawable.hotel1;
        }else{
            image_resource = R.drawable.hotel2;
        }

        // Segun la variable veo que layout de item usar, si el de lista o el de grid
        int layout = is_list ? R.layout.room_list_item : R.layout.grid_element;

        // Ojo si la vista no ha sido inflada debo inflarla, si no no
        if(convertView == null){
            convertView = inflater.inflate(layout, null);
            holder = new ViewHolder();
            holder.img = (ImageView) convertView.findViewById(R.id.img_row);
            holder.title = (TextView) convertView.findViewById(R.id.txt_row_title);
            holder.subtitle = (TextView) convertView.findViewById(R.id.txt_row_subtitle);

            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        // Ahora solo se trabaja con el HOLDER, lo que ahorra memoria
        holder.title.setText(current_room.getRoomNumber());
        holder.subtitle.setText(room_type);
        holder.img.setImageResource(image_resource);

        return convertView;
    }

    // Esta clase sirve pasa sostener una vista
    // y no tener que pintarla cada vez que se recorre el scroll
    private static class ViewHolder {
        public ImageView img;
        public TextView title;
        public TextView subtitle;
    }
}
