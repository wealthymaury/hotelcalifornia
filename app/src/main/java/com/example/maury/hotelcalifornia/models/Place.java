package com.example.maury.hotelcalifornia.models;

import com.example.maury.hotelcalifornia.R;
import com.google.android.gms.maps.model.LatLng;

public class Place {
    private int type;
    private String title;
    private double[] location;
    private String description;

    public final static int TYPE_HOSTEL = 0;
    public final static int TYPE_STANDARD = 1;
    public final static int TYPE_LUXURY = 2;

    public int getType(){
        return this.type;
    }

    public void setType(int type){
        this.type = type;
    }

    public int getResourceMarker(){
        int resource = 0;
        switch(type){
            case TYPE_HOSTEL:
                resource = R.drawable.ic_place_black_48dp;
                break;
            case TYPE_STANDARD:
                resource = R.drawable.ic_place_black_48dp;
                break;
            case TYPE_LUXURY:
                resource = R.drawable.ic_place_black_48dp;
                break;
        }

        return resource;
    }

    public double[] getLocation(){
        return this.location;
    }

    public LatLng getLatLng(){
        return new LatLng(location[0], location[1]);
    }

    public void setLocation(double[] location){
        this.location = location;
    }

    public String getTitle(){
        return this.title;
    }

    public String getDescription(){
        return this.description;
    }
}
