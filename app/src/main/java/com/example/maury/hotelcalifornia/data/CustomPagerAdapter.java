package com.example.maury.hotelcalifornia.data;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.maury.hotelcalifornia.fragments.PlacesMapFragment;
import com.example.maury.hotelcalifornia.fragments.RoomGridFragment;
import com.example.maury.hotelcalifornia.fragments.RoomListFragment;

/*
* Este adaptador sirve para gener aqui los fragmentos de los listados que se mostraran en los tabs
* */

public class CustomPagerAdapter extends FragmentStatePagerAdapter {
    private Fragment[] fragments;

    public CustomPagerAdapter(FragmentManager fm){
        super(fm);

        fragments = new Fragment[] {
                new RoomListFragment(),
                new RoomGridFragment(),
                new PlacesMapFragment()
        };
    }

    @Override
    public Fragment getItem(int position) {
        return fragments[position];
    }

    @Override
    public int getCount() {
        return fragments.length;
    }
}
