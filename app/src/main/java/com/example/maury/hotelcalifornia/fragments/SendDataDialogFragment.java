package com.example.maury.hotelcalifornia.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

import com.example.maury.hotelcalifornia.R;

/* Con esta clase muy especial es que podemos comunicar el dialogo con la activity a travez de los eventos de su interface */

public class SendDataDialogFragment extends DialogFragment {
    public interface DialogListener {
        void onDialogPositiveClick(DialogFragment dialog);
        void onDialogNegativeClick(DialogFragment dialog);
    }

    DialogListener listener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try{
            listener = (DialogListener) activity;
        }catch (ClassCastException e){}
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("");
        builder.setSingleChoiceItems(R.array.array_dialog_option, -1, null);
        builder.setPositiveButton(R.string.message_yes, new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                listener.onDialogPositiveClick(SendDataDialogFragment.this);
            }
        });
        builder.setNegativeButton(R.string.message_no, new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                listener.onDialogNegativeClick(SendDataDialogFragment.this);
            }
        });

        return builder.create();
    }
}
