package com.example.maury.hotelcalifornia.fragments;

import android.app.ActionBar;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.example.maury.hotelcalifornia.R;

public class TermsFragment extends Fragment {
    private WebView web_view;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_terms, container, false);
        web_view = (WebView) view.findViewById(R.id.web);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        web_view.loadUrl("http://www.google.com/");
        web_view.setWebViewClient(new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });

        ActionBar action_bar = getActivity().getActionBar();
        action_bar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
    }
}
