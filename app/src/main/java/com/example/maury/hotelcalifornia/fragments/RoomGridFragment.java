package com.example.maury.hotelcalifornia.fragments;

import android.support.v4.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.example.maury.hotelcalifornia.R;
import com.example.maury.hotelcalifornia.RoomDetailActivity;
import com.example.maury.hotelcalifornia.data.CustomAdapter;
import com.example.maury.hotelcalifornia.models.Room;

import java.util.ArrayList;

public class RoomGridFragment extends Fragment implements AdapterView.OnItemClickListener{
    private GridView grid;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_room_grid, container, false);
        grid = (GridView) view.findViewById(R.id.grid_rooms);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);

        ArrayList<Room> rooms = new ArrayList<>();

        // Recorro los ARRAY que tienen los cuartos y a partir de ellos lleno mi arraylist
        for(String room: getResources().getStringArray(R.array.array_rooms_standard)){
            Room one_room = new Room(room, Room.STANDARD_ROMM);
            rooms.add(one_room);
        }

        for(String room: getResources().getStringArray(R.array.array_rooms_luxury)){
            Room one_room = new Room(room, Room.LUXURY_ROMM);
            rooms.add(one_room);
        }

        // Creando el adapter con los datos y asignandolo a mi lista
        CustomAdapter adapter = new CustomAdapter(getActivity(), rooms, false);
        grid.setAdapter(adapter);

        // Haciendo que la clase que responde a los eventos de click sea esta misma, por el
        // implements del AdapterView.OnItemClickListener
        grid.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
        Room clicked_room = (Room) grid.getItemAtPosition(position);
        Intent intent = new Intent(getActivity(), RoomDetailActivity.class);
        intent.putExtra(RoomDetailActivity.ROOM_TYPE, clicked_room.getRoomType());
        intent.putExtra(RoomDetailActivity.ROOM_NUMBER, clicked_room.getRoomNumber());
        startActivity(intent);
    }
}
