package com.example.maury.hotelcalifornia;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.example.maury.hotelcalifornia.fragments.SendDataDialogFragment;
import com.example.maury.hotelcalifornia.models.Room;

public class RoomDetailActivity extends Activity implements SendDataDialogFragment.DialogListener {
    public static final String ROOM_TYPE = "tipo de habitacion";
    public static final String ROOM_NUMBER = "número de habitacion";
    public static final String TAG_DIALOG = "dialogo";

    private Room room;
    private boolean favorite = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_detail);

        Intent intent = getIntent();
        room = new Room(intent.getStringExtra(ROOM_NUMBER), intent.getStringExtra(ROOM_TYPE));

        ToggleButton toogle_recommendation = (ToggleButton) findViewById(R.id.toggle_recomendation);
        toogle_recommendation.setChecked(true);

        int resource;
        if(room.getRoomType().equals(Room.STANDARD_ROMM)){
            resource = R.drawable.hotel1;
        }else{
            resource = R.drawable.hotel2;
        }

        ImageView img_header = (ImageView) findViewById(R.id.img_header);
        img_header.setImageResource(resource);
        setTitle(room.getRoomNumber());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_fav:
                Drawable icon;
                if (favorite) {
                    icon = getResources().getDrawable(R.drawable.rating_not_important);
                }else {
                    icon = getResources().getDrawable(R.drawable.rating_important);
                }
                favorite = !favorite;
                item.setIcon(icon);
                return true;
            case R.id.action_share:
                // Obteniendo el mensaje por defecto para compartit
                String message = getResources().getString(R.string.message_share);
                // Obteniendo la url del recurso
                Uri img_resource = Uri.parse("android.resource://" + getPackageName() + "/drawable/" + R.drawable.hotel1);
                // Creando el intent para lanzar el modal de compartir, se le pasa un texto y el recurso
                Intent share = new Intent();
                share.setAction(Intent.ACTION_SEND);
                share.putExtra(Intent.EXTRA_TEXT, message);
                share.putExtra(Intent.EXTRA_STREAM, img_resource);
                share.setType("image/jpeg");
                // Lanzando el modal de compartir
                startActivity(Intent.createChooser(share, getResources().getString(R.string.title_share)));
                return true;
            case R.id.action_dialog:
                // Instanciamos el dialogo perron que creamos y lo mostramos
                SendDataDialogFragment f = new SendDataDialogFragment();
                f.show(getFragmentManager(), RoomDetailActivity.TAG_DIALOG);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void toggleClicked(View v){
        Toast.makeText(getApplicationContext(), "Toggle", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {
        Toast.makeText(getApplicationContext(), "Dijo que si", Toast.LENGTH_SHORT).show();
        Log.e("TAG", "Dijo que si");
    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {
        Toast.makeText(getApplicationContext(), "Dijo que no", Toast.LENGTH_SHORT).show();
        Log.e("TAG", "Dijo que no");
    }
}
